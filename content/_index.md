+++
title = "Grundschule Lindau-Insel"

# The homepage contents
[extra]
lead = 'Barfüßerplatz 5<br>88131 Lindau am Bodensee<br>Telefon: <a href="tel:+498382944521">08382-944521</a><br>Email: <a href="mailto:GS-Insel@gmx.de">GS-Insel@gmx.de</a>'

[[extra.list]]
title = "Leitbild"
content = 'Lorem ipsum'
icon = 'fa-lightbulb'

[[extra.list]]
title = "Warum Inselgrundschule"
content = '* Nah * Persönlich * ...'
icon = 'fa-question'

[[extra.list]]
title = "Schulgebäude"
content = '* Bilder: Gebäude, Räume, Sporthalle, ...'
icon = 'fa-school'

[[extra.list]]
title = "Kollegium"
content = "Regula Metzenthin: Schulleitung<br>Barbara Einsiedler: Verwaltungsangestellte<br>Thomas Wucher: Hausmeister<br>Frau Kranz: Klasse 1/2<br>Frau Metzenthin: Klasse 3<br>Frau Keck: Klasse 4<br>Frau Sellwig<br>Frau Lindner<br>Frau Fasser<br>Frau Dürrschmidt (WG)"
icon = 'fa-chalkboard-teacher'

[[extra.list]]
title = "Stundenpläne"
content = 'Stundenplan der 1./2./3./4. Klasse'
icon = 'fa-calendar-alt'

[[extra.list]]
title = "Schulanmeldung"
content = '* Informationen zur Anmeldung: Wann, Wo, Wie'
icon = 'fa-calendar-check'

[[extra.list]]
title = "Gastschulanträge"
content = '* Informationen zu Gastschulantrag: Wer, Wo, Wie'
icon = 'fa-calendar-plus'

[[extra.list]]
title = "Lehrplan"
content = '* Informationen zu den Lehrplänen'
icon = 'fa-clipboard-list'

[[extra.list]]
title = "Elternbeirat"
content = 'Elternbeirat im Schuljahr 2020/21:<br><br>Elke Horber: 1. Vorsitzende<br>Karoline Zuderell: 2. Vorsitzende<br>Estibaliz Ortiz de Zarate Gutierrez: Kassier<br>Katrin Rellstab: Schriftführer<br><br>Email: <a href="mailto:eb-grundschule_insel@gmx.de">eb-grundschule_insel@gmx.de</a>'
icon = 'fa-users'

+++
