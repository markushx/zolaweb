+++
title = "Mittagsbetreuung"
description = "Betreuung von Schulschluss bis 14:00"
date = 2021-05-01T18:10:00+00:00
updated = 2021-05-01T18:10:00+00:00
draft = false
weight = 410
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "des Fördervereins der Mittagsbetreuung"
toc = true
top = false
+++

## Betreuungszeiten

Verlässliche Betreuung von Unterrichtsschluss bis 14.00 Uhr.

Flexible Abholungszeiten nach Vereinbarung.

## Kosten

60 € / Monat

Für den Monat August fallen keine Kosten an.

## Leistungen

Obst, Tee, Müsli und Milch inklusive.

## Räumlichkeiten

Der Mittagsbetreuung stehen zwei kindgerecht möblierte Räume mit zahlreichem Baumaterial, Spielen, Büchern, sowie jahreszeitlichem Bastelmaterial in der Grundschule zur Verfügung.

Bewegung, Spiel und Spaß finden die Kinder auch auf dem Pausenhof und in der Turnhalle.

## Kontakt

Frau Ramona Krause, Tel. 0151/28850475

## Kinder der 1. Klasse

Die ersten Schulwochen werden die Kinder zuverlässig von der Betreuerin nach Schulschluss am Klassenzimmer abgeholt. Anfangs sind manche Kinder noch etwas unsicher und brauchen besondere individuelle Zuwendung. Sie werden unterstützt und ermutigt an der Gemeinschaft teilzunehmen. Es dauert nur kurze Zeit, bis auch die Kinder der 1. Klasse mit den anderen Kindern eine fröhliche Gemeinschaft bilden.

## Konzept

Die Mittagsbetreuung ist ein Lebensraum, in dem die Kinder soziale Erfahrungen sammeln können.

Den Kindern wird eine offene, auf ihre Individualität eingehende Betreuung ermöglicht. Es werden bewusst keine Projekte verfolgt, die Kinder sollen sich nach Unterrichtsschluss ihren individuellen Bedürfnissen entsprechend ausleben und entspannen. So können sie Emotionen abbauen und neue Energie schöpfen.

Die Art der Beschäftigung wählen die Kinder möglichst selbst und werden darin von den Betreuerinnen unterstützt. Impulse der Kinder werden aufgegriffen und in die Beschäftigungsangebote mit einbezogen. Diese Impulse kommen auch aus dem Unterricht, die dann in der Mittagsbetreuung ausgelebt werden (Lieder, Gedichte, Kunstbilder….)

Den Kindern soll einerseits die erforderliche Entspannung und Ruhe nach dem Unterricht ermöglicht werden, andererseits aber auch Gelegenheit geboten werden, allein oder im Umgang mit anderen zu spielen, kreativ tätig zu sein, positives soziales Verhalten zu üben und ihre Freizeit sinnvoll zu gestalten.

Im Umgang miteinander werden gemeinsam mit den Kindern Regeln entwickelt (und fortlaufend weiterentwickelt), an denen sich die Kinder orientieren können und die faire Konfliktlösungen ermöglichen sollen. Gemeinsam werden die Kinder bewusst durch das Jahr begleitet - Geburtstag und Feste werden gefeiert. Gemeinsam werden verschiedene Ausflüge in die Umgebung unternommen.

Damit die pädagogische Arbeit gelingt, arbeiten die Betreuerinnen stets mit Eltern und Lehrern gut zusammen.

## Verein

* Mitarbeit erwünscht
* Kontaktdaten
* Mitglied werden
